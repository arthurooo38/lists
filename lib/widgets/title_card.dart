import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:lists/settings_page.dart';
import 'package:lists/variables.dart';
import 'package:provider/provider.dart';
import 'package:hive/hive.dart';
import 'package:ionicons/ionicons.dart';

class TitleCard extends StatelessWidget {
  const TitleCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<MyLists>(builder: (context, myLists, child) {
      var box = Hive.box<List<dynamic>>("listsBox");
      myLists.severalLists = box.get("lists") ?? [];
      return Container(
        height: 100,
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: borderRadius*2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Text(
                "Lists",
                style: TextStyle(
                  color: Theme.of(context).colorScheme.onBackground,
                  fontSize: 28,
                  fontWeight: FontWeight.w600, letterSpacing: 1,
                ),
              ),
            ),
            IconButton(
              icon: Icon(Ionicons.ellipsis_vertical),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute<void>(
                builder: (BuildContext context) => const SettingsPage()));
              },
            )
          ],
        ),
      );
    });
  }
}
