import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lists/variables.dart';
import 'package:provider/provider.dart';
import 'package:hive/hive.dart';
import 'package:ionicons/ionicons.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';

class ItemsList extends StatefulWidget {
  ItemsList({Key? key}) : super(key: key);

  @override
  State<ItemsList> createState() => _ItemsListState();
}

class _ItemsListState extends State<ItemsList> {
  String _txt = "";

  @override
  Widget build(BuildContext context) {
    return Consumer<MyLists>(builder: (context, myLists, child) {
      var box = Hive.box<List<dynamic>>("listsBox");
      myLists.severalLists = box.get("lists") ?? [];
      return myLists.severalLists.isEmpty
          ? IconButton(
              iconSize: 128,
              onPressed: () {
                MyLists().get();
                showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(borderRadius)),
                    backgroundColor: Theme.of(context).colorScheme.background,
                    title: const Text(""),
                    content: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(borderRadius),
                            borderSide: BorderSide.none),
                        filled: true,
                        fillColor: Theme.of(context).colorScheme.surface,
                      ),
                      textCapitalization: TextCapitalization.sentences,
                      onChanged: (txt) {
                        _txt = txt;
                      },
                    ),
                    actions: [
                      TextButton(
                        child: const Text("OK"),
                        onPressed: () {
                          if (_txt != "") {
                            myLists.createList(_txt);
                            _txt = "";
                            Navigator.pop(context);
                          }
                        },
                      ),
                    ],
                  ),
                );
              },
              icon: Icon(Ionicons.add_circle_outline,
                  color: Theme.of(context).colorScheme.primary))
          : Column(
              children: [
                Divider(
                  height: 0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.surface),
                  height: 100,
                  padding: EdgeInsets.symmetric(
                      horizontal: borderRadius, vertical: 20),
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: myLists.severalLists.length + 1,
                      itemBuilder: (BuildContext context, int index) {
                        return index == myLists.severalLists.length
                            ? Padding(
                                padding: const EdgeInsets.all(8),
                                child: TextButton(
                                  onPressed: () {
                                    MyLists().get();
                                    showDialog(
                                      context: context,
                                      builder: (_) => AlertDialog(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                borderRadius)),
                                        backgroundColor: Theme.of(context)
                                            .colorScheme
                                            .background,
                                        title: const Text(""),
                                        content: TextField(
                                          autofocus: true,
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        borderRadius),
                                                borderSide: BorderSide.none),
                                            filled: true,
                                            fillColor: Theme.of(context)
                                                .colorScheme
                                                .surface,
                                          ),
                                          textCapitalization:
                                              TextCapitalization.sentences,
                                          onChanged: (txt) {
                                            _txt = txt;
                                          },
                                        ),
                                        actions: [
                                          TextButton(
                                            child: const Text("OK"),
                                            onPressed: () {
                                              if (_txt != "") {
                                                myLists.createList(_txt);
                                                _txt = "";
                                                Navigator.pop(context);
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                  style: TextButton.styleFrom(
                                    padding: EdgeInsets.all(10),
                                    backgroundColor:
                                        Theme.of(context).colorScheme.secondary,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(
                                            borderRadius)),
                                  ),
                                  child: Text(
                                    "+",
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .onSecondary),
                                  ),
                                ),
                              )
                            : Padding(
                                padding: const EdgeInsets.all(8),
                                child: TextButton(
                                  onLongPress: () {
                                    showDialog(
                                      context: context,
                                      builder: (_) {
                                        String _txt =
                                            myLists.severalLists[index].title;
                                        return AlertDialog(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      borderRadius)),
                                          backgroundColor: Theme.of(context)
                                              .colorScheme
                                              .background,
                                          title: const Text(
                                              "Try editing the title"),
                                          content: TextFormField(
                                            initialValue: _txt,
                                            decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          borderRadius),
                                                  borderSide: BorderSide.none),
                                              filled: true,
                                              fillColor: Theme.of(context)
                                                  .colorScheme
                                                  .surface,
                                            ),
                                            textCapitalization:
                                                TextCapitalization.sentences,
                                            onChanged: (txt) {
                                              _txt = txt;
                                            },
                                          ),
                                          actions: [
                                            TextButton(
                                              child: const Text("DELETE"),
                                              style: TextButton.styleFrom(
                                                  primary: Theme.of(context)
                                                      .colorScheme
                                                      .error),
                                              onPressed: () {
                                                myLists.deleteList(index);
                                                Navigator.pop(context);
                                              },
                                            ),
                                            TextButton(
                                              child: const Text("SAVE"),
                                              onPressed: () {
                                                myLists.editTitle(_txt, index);
                                                Navigator.pop(context);
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                  style: TextButton.styleFrom(
                                    padding: EdgeInsets.all(10),
                                    backgroundColor: myLists.activeList == index
                                        ? Theme.of(context).colorScheme.primary
                                        : Theme.of(context)
                                            .colorScheme
                                            .secondary,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(
                                            borderRadius)),
                                  ),
                                  onPressed: () {
                                    myLists.changeList(index);
                                  },
                                  child: Text(
                                    myLists.severalLists[index].title,
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: myLists.activeList == index
                                            ? FontWeight.w500
                                            : FontWeight.w400,
                                        color: myLists.activeList == index
                                            ? Theme.of(context)
                                                .colorScheme
                                                .onPrimary
                                            : Theme.of(context)
                                                .colorScheme
                                                .onSecondary),
                                  ),
                                ),
                              );
                      }),
                ),
                Divider(
                  height: 0,
                ),
                myLists.severalLists[myLists.activeList].items.isEmpty
                    ? Expanded(
                        child: Center(
                          child: Container(
                              padding: EdgeInsets.all(32),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(borderRadius),
                                  color: Theme.of(context).colorScheme.surface),
                              child: Icon(
                                Ionicons.documents_outline,
                                size: 96,
                                color: Theme.of(context).colorScheme.onSurface,
                              )),
                        ),
                      )
                    : Expanded(
                        child: Container(
                          margin: EdgeInsets.all(borderRadius),
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    offset: Offset(0, 1),
                                    blurRadius: 1)
                              ],
                              color: Theme.of(context).colorScheme.surface,
                              borderRadius:
                                  BorderRadius.circular(borderRadius)),
                          child: ListView.builder(
                              padding:
                                  EdgeInsets.symmetric(vertical: borderRadius),
                              itemCount: myLists
                                  .severalLists[myLists.activeList]
                                  .items
                                  .length,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  key: UniqueKey(),
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  child: Dismissible(
                                    key: Key(myLists
                                        .severalLists[myLists.activeList]
                                        .items[index]),
                                    onDismissed: (direction) {
                                      myLists.removeItem(index);
                                    },
                                    background: Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20),
                                      decoration: BoxDecoration(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .error),
                                      child: Row(
                                        children: [
                                          Icon(Ionicons.close_circle_outline,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .onError),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          Expanded(
                                              child: Text(
                                            "DELETE",
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .colorScheme
                                                    .onError,
                                                fontWeight: FontWeight.w600),
                                          )),
                                        ],
                                      ),
                                    ),
                                    secondaryBackground: Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20),
                                      decoration: BoxDecoration(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .error),
                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: Text("DELETE",
                                                  textAlign: TextAlign.end,
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .colorScheme
                                                          .onError,
                                                      fontWeight:
                                                          FontWeight.w600))),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          Icon(Ionicons.close_circle_outline,
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .onError),
                                        ],
                                      ),
                                    ),
                                    child: TextButton(
                                      style: TextButton.styleFrom(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 6, horizontal: 6),
                                      ),
                                      onPressed: () {
                                        myLists.checkItem(index);
                                        Vibrate.feedback(
                                            FeedbackType.selection);
                                      },
                                      onLongPress: () {
                                        showDialog(
                                          context: context,
                                          builder: (_) {
                                            String _txt = myLists
                                                .severalLists[myLists.activeList].items[index];
                                            return AlertDialog(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          borderRadius)),
                                              backgroundColor: Theme.of(context)
                                                  .colorScheme
                                                  .background,
                                              title: const Text(
                                                  "Try editing the text"),
                                              content: TextFormField(
                                                initialValue: _txt,
                                                decoration: InputDecoration(
                                                  border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              borderRadius),
                                                      borderSide:
                                                          BorderSide.none),
                                                  filled: true,
                                                  fillColor: Theme.of(context)
                                                      .colorScheme
                                                      .surface,
                                                ),
                                                textCapitalization:
                                                    TextCapitalization
                                                        .sentences,
                                                onChanged: (txt) {
                                                  _txt = txt;
                                                },
                                              ),
                                              actions: [
                                                TextButton(
                                                  child: const Text("SAVE"),
                                                  onPressed: () {
                                                    myLists.editItem(
                                                        _txt, myLists.activeList, index);
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      },
                                      child: Row(
                                        children: [
                                          Container(
                                            child: Icon(myLists
                                                .severalLists[
                                            myLists.activeList]
                                                .checks[index]
                                                ? Ionicons.checkbox_outline : Ionicons.square_outline, size: 22),
                                            margin: EdgeInsets.symmetric(
                                                horizontal: borderRadius,
                                                vertical: 8),
                                          ),
                                          Expanded(
                                            child: AnimatedDefaultTextStyle(
                                              duration:
                                                  Duration(milliseconds: 200),
                                              style: TextStyle(
                                                  fontSize: 17,
                                                  color: myLists
                                                          .severalLists[myLists
                                                              .activeList]
                                                          .checks[index]
                                                      ? Theme.of(context)
                                                          .colorScheme
                                                          .onBackground.withOpacity(.4)
                                                      : Theme.of(context)
                                                          .colorScheme
                                                          .onBackground),
                                              child: Text(
                                                myLists
                                                    .severalLists[
                                                        myLists.activeList]
                                                    .items[index],
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ),
              ],
            );
    });
  }
}
