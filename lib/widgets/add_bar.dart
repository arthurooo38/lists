import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:lists/variables.dart';
import 'package:hive/hive.dart';


class AddBar extends StatelessWidget {
  AddBar({Key? key}) : super(key: key);

  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer<MyLists>(
      builder: (context, myLists, child) {
        var box = Hive.box<List<dynamic>>("listsBox");
        myLists.severalLists = box.get("lists")?? [];
        return myLists.severalLists.isEmpty? Container(): Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.all(borderRadius), padding: EdgeInsets.symmetric(horizontal: 20, vertical: 4),
          decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Colors.black12, offset: Offset(0, 1), blurRadius: 1)],
            borderRadius: BorderRadius.circular(borderRadius),
            color: Theme.of(context).colorScheme.surface
          ),

          child: TextField(
            controller: _controller,
            textCapitalization: TextCapitalization.sentences,
            style: TextStyle(color: Theme.of(context).colorScheme.onSurface),
            decoration: InputDecoration(
              hintText: "Add something ... ",
              hintStyle: TextStyle(color: Theme.of(context).colorScheme.onSurface),
              border: InputBorder.none
            ),
            onSubmitted: (txt) {
              myLists.addItem(txt);
              _controller.clear();
            },
            textInputAction: TextInputAction.done,
          )
        );
      }
    );
  }
}
