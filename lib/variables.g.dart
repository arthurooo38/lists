// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'variables.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class OneListAdapter extends TypeAdapter<OneList> {
  @override
  final int typeId = 0;

  @override
  OneList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return OneList(
      fields[1] as String,
      (fields[2] as List).cast<dynamic>(),
    )
      ..created = fields[0] as DateTime
      ..checks = (fields[3] as List).cast<dynamic>();
  }

  @override
  void write(BinaryWriter writer, OneList obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.created)
      ..writeByte(1)
      ..write(obj.title)
      ..writeByte(2)
      ..write(obj.items)
      ..writeByte(3)
      ..write(obj.checks);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OneListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
