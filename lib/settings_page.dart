import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lists/variables.dart';
import 'package:url_launcher/url_launcher_string.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);

  Future<void> _launchUrl(_url) async {
    if (!await launchUrl(_url, mode: LaunchMode.externalApplication)) {
      throw 'Could not launch $_url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              height: 100,
              padding: EdgeInsets.symmetric(
                  vertical: 24, horizontal: borderRadius * 2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "More ...",
                      style: TextStyle(
                        color: Theme.of(context).colorScheme.onBackground,
                        fontSize: 28,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1,
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Ionicons.arrow_back_outline),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(28),
              child: Text(
                "Hello\n"
                "I'm Arthur, the developper behind this app. "
                "Hope you will like it !",
                style: TextStyle(fontSize: 16),
                textAlign: TextAlign.start,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: borderRadius * 2, vertical: borderRadius / 2),
              child: TextButton(
                onPressed: () {
                  _launchUrl(Uri.parse("https://codeberg.org/arthurooo38/lists"));
                },
                child: Padding(
                  padding: EdgeInsets.all(borderRadius),
                  child: Row(
                    children: [
                      Icon(Ionicons.code_slash_outline),
                      Text("   Source code"),
                    ],
                  ),
                ),
                style: TextButton.styleFrom(
                  padding: EdgeInsets.all(10),
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(borderRadius)),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: borderRadius * 2, vertical: borderRadius / 2),
              child: TextButton(
                onPressed: () {
                  _launchUrl(Uri.parse("https://www.buymeacoffee.com/arthurooo38"));
                },
                child: Padding(
                  padding: EdgeInsets.all(borderRadius),
                  child: Row(
                    children: [
                      Icon(Ionicons.cafe_outline),
                      Text("   Support me"),
                    ],
                  ),
                ),
                style: TextButton.styleFrom(
                  padding: EdgeInsets.all(10),
                  backgroundColor: Theme.of(context).colorScheme.secondary,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(borderRadius)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
