import 'package:flutter/material.dart';
import 'package:lists/home.dart';
import 'package:provider/provider.dart';
import 'package:lists/variables.dart';
import 'package:hive_flutter/hive_flutter.dart';

void  main() async {

  await Hive.initFlutter();
  Hive.registerAdapter(OneListAdapter());
  await Hive.openBox<List<dynamic>>('listsBox');

  runApp(ChangeNotifierProvider<MyLists>(
    create: (context) => MyLists(),
    child: Builder(
      builder: (context) {
        return const MyApp();
      }
    ),
  )
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          colorScheme: const ColorScheme(
              background: Color(0xffF7F9FF), onBackground: Color(0xFF00005F),
              onError: Colors.white, error: Color(0xfffa7887),
              primary: Color(0xff668EE3), onPrimary: Color(0xffF6F5FC),
              surface: Color(0xFFFFFFFF), onSurface: Color(0xFF00005F),
              secondary: Color(0xffF3F3F3), onSecondary: Color(0xffBFBFBF),
              brightness: Brightness.light,
              primaryVariant: Color(0xffF7F9FF), secondaryVariant: Color(0xffEAEFFF)
          ),
          fontFamily: "Rubik"
      ),
        darkTheme: ThemeData(
            colorScheme: const ColorScheme(
                background: Color(0xff000000), onBackground: Color(0xFFF7F9FF),
                onError: Colors.white, error: Color(0xfffa7887),
                primary: Color(0xffC2D5FE), onPrimary: Color(0xFF00005F),
                secondary: Color(0xff000000), onSecondary: Color(0xff808080),
                surface: Color(0xff161616), onSurface: Color(0xffF7F9FF),
                brightness: Brightness.dark,
                primaryVariant: Color(0xff07132D), secondaryVariant: Color(0xff07132D)
            ),
            fontFamily: "Rubik"
        ),
      home: const HomePage()
    );
  }
}