import 'package:flutter/material.dart';
import 'package:lists/settings_page.dart';
import 'package:lists/variables.dart';
import 'package:lists/widgets/title_card.dart';
import 'package:lists/widgets/list_view.dart';
import 'package:lists/widgets/add_bar.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:ionicons/ionicons.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _txt = "";


  @override
  Widget build(BuildContext context) {
          return Scaffold(
            backgroundColor: Theme.of(context).colorScheme.background,
            body: SafeArea(
                child: Column(
                  children: [
                    const TitleCard(),
                    Expanded(child: ItemsList()),
                    AddBar()
                  ],
                )
            ),
          );
  }
}
