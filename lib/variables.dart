import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
part 'variables.g.dart';


double borderRadius = 16;


@HiveType(typeId: 0)
class OneList{
  @HiveField(0)
  late DateTime created = DateTime.now();
  @HiveField(1)
  late String title = "Title";
  @HiveField(2)
  late List items = [];
  @HiveField(3)
  late List checks = [];

  OneList(this.title, this.items);
}

class MyLists extends ChangeNotifier{
  var box = Hive.box<List<dynamic>>('listsBox');
  List<dynamic> severalLists = [];
  int activeList = 0;


  void get() {
    severalLists = box.get("lists")?? [];
    print("ok");
  }

  void addItem(String txt) {
    if(txt !="") {severalLists[activeList].items.add(txt); severalLists[activeList].checks.add(false);}
    notifyListeners();
    box.put("lists", severalLists);
  }

  void removeItem(int i) {
    severalLists[activeList].items.removeAt(i);
    severalLists[activeList].checks.removeAt(i);
    notifyListeners();
    box.put("lists", severalLists);
  }

  void checkItem(int i) {
    severalLists[activeList].checks[i] = !severalLists[activeList].checks[i];
    notifyListeners();
    box.put("lists", severalLists);
  }

  void changeList(int i) {
    activeList = i;
    notifyListeners();
    box.put("lists", severalLists);
  }

  void createList(String title) {
    severalLists.add(OneList(title, []));
    activeList = severalLists.length-1;
    notifyListeners();
    box.put("lists", severalLists);
  }

  void deleteList(int i) {
    severalLists.removeAt(i);
    if(activeList==i) {activeList=0;}
    if(activeList>=severalLists.length) {activeList=0;}
    notifyListeners();
    box.put("lists", severalLists);
  }

  void editTitle(String new_title, int index) {
    severalLists[index].title = new_title;
    notifyListeners();
    box.put("lists", severalLists);
  }

  void editItem(String new_txt, int indexList, int indexItem) {
    severalLists[indexList].items[indexItem] = new_txt;
    notifyListeners();
    box.put("lists", severalLists);
  }

}


