# LISTS

This is an opensource app made for Android.
- Made with Flutter
- Simple, fast and a modern UI

The app simply allow you to make lists about whatever you want.

### Features
- Create lists easily to stay organized
- Check items when you are done !
- Dark and light mode

### Screenshots
<img height="400" src="1.png"/>
<img height="400" src="2.png"/>

### Get it and support

[![support-me](https://www.buymeacoffee.com/assets/img/guidelines/download-assets-sm-1.svg)](https://www.buymeacoffee.com/arthurooo38)

### Contribution

Feel free to contribute !